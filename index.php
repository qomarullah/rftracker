<?php
session_start();
/*echo "aaaaaaa".$_SESSION['username'];
echo isset($_SESSION['username']);
exit();
*/
if(isset($_SESSION['username'])){
	$username=$_SESSION['username'];
	$userid=$_SESSION['userid'];
	$type=$_SESSION['type'];
	
}else{
  header('location: login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=0.5">
    <meta name="description" content="">
    <meta name="author" content="">
-->
    <title>RF Tracker</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="css/freelancer.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
	-->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">
    
	
<!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="title">
				<h2><a href="index.php">Vodacom RF Tracker</a></h2>
            </div>
			<div class="title">
                <a href="index.php?page=rf_batch">Batch Input</a>
				 |&nbsp; <a href="index.php?page=rf_tracker">Show Data</a>
				<?php
				if($type){
					echo "|&nbsp;<a href=\"index.php?page=user\">User</a>";
				}
				?>
				&nbsp;|&nbsp;<a href="logout.php">Logout</a>
				
            </div>

            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
   </nav>
	    <!-- Header -->
    

	<?php
	//include("config.php");
	
	$page=$_REQUEST['page'];
	$table=$_REQUEST['table'];
	if($page==""){
		$page="rf_tracker";
	}
	if($page!=""){
		
		echo "<section id=\"content\">
			<div class=\"container\">";
			
		include "pages/".$page.".php";
	
		echo "</div></section>";
		
	}else{
		include "pages/default.php";
		echo "<script src=\"vendor/jquery/jquery.min.js\"></script>";
		echo "<script src=\"vendor/bootstrap/js/bootstrap.min.js\"></script>";
	}
	
	?>
    <!-- Footer -->
    <footer class="text-left">
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; Huawei 
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
<!--    <script src="vendor/jquery/jquery.min.js"></script>
-->
    <!-- Bootstrap Core JavaScript -->
<!--    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
-->

</body>

</html>
