/*
SQLyog Community v12.2.6 (64 bit)
MySQL - 10.1.22-MariaDB : Database - sites
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sites` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sites`;

/*Table structure for table `rf_tracker` */

DROP TABLE IF EXISTS `rf_tracker`;

CREATE TABLE `rf_tracker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region` varchar(10) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `cluster_name` varchar(50) DEFAULT NULL,
  `site_name` varchar(50) DEFAULT NULL,
  `cell_name` varchar(50) DEFAULT NULL,
  `ci` double DEFAULT NULL,
  `PSC` double DEFAULT NULL,
  `height` double DEFAULT NULL,
  `sonar_db_azimuth` double DEFAULT NULL,
  `sonar_db_electilt` double DEFAULT NULL,
  `sonar_db_mechtilt` double DEFAULT NULL,
  `plan` text,
  `before_azimuth` double DEFAULT NULL,
  `before_electilt` double DEFAULT NULL,
  `before_mechtilt` double DEFAULT NULL,
  `after_azimuth` double DEFAULT NULL,
  `after_electilt` double DEFAULT NULL,
  `after_mechtilt` double DEFAULT NULL,
  `ret_status` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rf_tracker` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`phone`,`email`,`password`,`type`,`last_update`) values 
(1,'admin','08118003585','qomarullah.mail@gmail.comxx','0192023a7bbd73250516f069df18b500',1,'2017-06-28 20:32:54'),
(2,'user','08118003585','qomarullah.mail@gmail.com','6ad14ba9986e3615423dfca256d04e3f',0,'2017-06-28 20:34:50');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
